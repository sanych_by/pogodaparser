#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QRegExp>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    auto filename = QFileDialog::getOpenFileName(
                this, tr("Выберите сохранённую страницу"));
    if(!filename.isEmpty())
    {
        QFile file(filename);
        if(file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QFile result("result.csv");
            result.open(QIODevice::WriteOnly | QIODevice::Text);
            result.write(QString("Дата;Время;Температура;Влажность;Давление;Грунт;"
                                 "Температура после ГТ;Разница с комфортом;Эффективность\n").toUtf8());
            QString text = file.readAll();
            QRegExp dailyStat("<span class=\"future-day\">(\\d{1,2} \\w+)</span>"
                              "(.+)Восход в (\\d\\d:\\d\\d).+"
                              "Закат в (\\d\\d:\\d\\d)");
            QRegExp periodStat("<div class=\"future-h\">(Утром|Днем|Вечером|Ночью)"
                               "</div></div>.+"
                               "<span class=\"temp-i\">(.+)°</span>.+"
                               "<div class=\"detail-i\"> Влажность (\\d{1,3}) % </div>.+"
                               "<div class=\"detail-i\">Давление (\\d{3}) мм рт. ст<br>"
                               ".+Ветер");
            dailyStat.setMinimal(true);
            periodStat.setMinimal(true);
            int index = dailyStat.indexIn(text, 0);
            int countP = 0, countM = 0;
            double sumEff = 0, sumP=0;
            while(index > -1)
            {
                qDebug() << dailyStat.cap(1) << dailyStat.cap(3) << dailyStat.cap(4);
                QString stat = dailyStat.cap(2);
                int periodInd = periodStat.indexIn(stat);
                while(periodInd > -1)
                {
                    qDebug() << dailyStat.cap(1) << periodStat.cap(1) << "темпер" <<
                                periodStat.cap(2) << "влажность" << periodStat.cap(3);
                    //qDebug() << periodStat.cap(0);
                    int temper = periodStat.cap(2).toInt();
                    int ground = ui->ground->value();
                    int diffGround = abs(temper-ground)/ground;
                    ground = ground<temper?ground+diffGround:ground-diffGround;
                    //double effectivity = 1 - abs(temper-ground)
                    int diff = ui->maxDiff->value();
                    int after = temper > (ground+diff)? temper-diff:
                                                        temper < (ground-diff)?
                                                            temper+diff:ground;
                    int need = ui->setTemper->value();
                    int diffe = abs(need-after);
                    int diffw = abs(need-temper);
                    bool good = diffw-diffe > 0;
                    double eff = need/static_cast<double>(need+diffe);
                    eff *= good?1:-1;
                    sumEff += eff;
                    if(good)
                    {
                        countP++;
                        sumP+=eff;
                    }
                    else {
                        countM++;
                    }
                    result.write(QString("%1;%2;%3;%4;%5;%6;%7;%8;%9\n")
                                 .arg(dailyStat.cap(1),  periodStat.cap(1),
                                       periodStat.cap(2),  periodStat.cap(3),
                                      periodStat.cap(4))
                                 .arg(ground)
                                 .arg(after)
                                 .arg(diffe)
                                 .arg(eff).toUtf8());
                    //qDebug() << "Looking in" << stat.mid(periodInd+periodStat.cap(0).length());
                    periodInd = periodStat.indexIn(stat, periodInd+periodStat.cap(0).length());
                }
                index = dailyStat.indexIn(text, index+dailyStat.cap(0).length());
            }
            result.write(QString("\n%1;%2;%3;%4;\n")
                         .arg(countP)
                         .arg(countM)
                         .arg(sumEff)
                         .arg(sumP/(countP)).toUtf8());
            file.close();
            result.close();
        }
    }
}
